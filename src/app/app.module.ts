import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './page/home/home.component';
import { WalletComponent } from './page/wallet/wallet.component';
import { IsLoggedGuard } from './guard/is-logged.guard';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WalletComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [IsLoggedGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
