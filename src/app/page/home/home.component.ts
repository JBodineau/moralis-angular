import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  constructor(private router: Router){}

  public async loginMetamask() {
    let user = Moralis.User.current();
    if (!user || !user.get("ethAddress")) {
      user = await Moralis.Web3.authenticate();
    }
    console.log("logged in user:", user);
    this.goWallet();
  }

  public async loginElrond() {
    let user = Moralis.User.current();
    if (!user || !user.get("erdAddress")) {
      user = await Moralis.authenticate({ type: 'elrond' });
    }
    console.log("logged in user:", user);
    this.goWallet();
  }

  public async loginPolkadot() {
    let user = Moralis.User.current();
    if (!user || !user.get("dotAddress")) {
      user = await Moralis.authenticate({ type: 'polkadot' });
    }
    console.log("logged in user:", user);
    this.goWallet();
  }

  public goWallet() {
    this.router.navigateByUrl('/wallet');
  }

}
