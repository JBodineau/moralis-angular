import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html'
})
export class WalletComponent implements OnInit{

  constructor(private router: Router){}

  public ethAddress: string = '';
  public egldAddress: string = '';
  public transactions: number = 0;
  public balance: number = 0;

  ngOnInit(): void {
      this.initDatas();
  }

  async logout() {
    await Moralis.User.logOut();
    console.log("logged out");
    this.goHome();
  }

  public goHome() { 
    this.router.navigateByUrl('/home');
  }

  async initDatas() {
    //Since this component is protected by the 'is-logged' guard, we are sure the user is authenticated.
    const user = Moralis.User.current();
    this.ethAddress = user.get("ethAddress");
    this.egldAddress = user.get("erdAddress");
    let queryTx: Query, queryAddress: Query;
    if(this.ethAddress) {
      //If eth address is defined, we are logged via metamask

      //Init Transactions query
      queryTx = new Moralis.Query("EthTransactions");
      queryTx.equalTo("from_address", user.get("ethAddress"));

      //Get ETH balance
      queryAddress = new Moralis.Query("_EthAddress");
      const ethAddress: any = await queryAddress.find();
      this.balance =  ethAddress[0].get("balanceEth");

    } else if(this.egldAddress){
      //If erd address is defined, we are logged via ledger

      //Init Transactions query
      queryTx = new Moralis.Query("ErdTransactions");
      queryTx.equalTo("from_address", user.get("erdAddress"));

      //Get ERD balance
      queryAddress = new Moralis.Query("_ErdAddress");
      const erdAddress: any = await queryAddress.find();
      this.balance =  erdAddress[0].get("balanceErd");

    } else {
      console.log("Address not found!"); 
      return;
    }
  
    // run Transaction query
    const results = await queryTx.find();
    console.log("user transactions:", results);
    this.transactions = results.length;
  }

}
