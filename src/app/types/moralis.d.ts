// Type definitions for Moralis
declare interface Moralis {

    initialize(applicationId: string, javaScriptKey?: string): void;
    authenticate(options: { type: string; }): Promise<User>;
    serverURL: string;
    liveQueryServerURL: string;
    User: User;
    Web3: Web3;
    Query: Query;
}

declare interface User {

    current(): User;
    logOut(): void;
    get(field: string): string;

}

declare interface Web3 {

    authenticate(): Promise<User>;
    
}

declare interface Query {

    new(type: string): Query;
    equalTo(field: string, value: string): void;
    find(): Promise<Array<any>>;
}



declare var Moralis: Moralis;