#########################
### build environment ###
#########################

# base image
FROM node:14.16.1 as builder

# create working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

# install and cache app dependencies
RUN npm install -g @angular/cli@11.2.9 --unsafe

# add app and npm dependencies
COPY . /usr/src/app
RUN npm install

# generate build
RUN ng build --prod

##################
### production ###
##################

# base image
FROM nginx:1.19.10-alpine
COPY ./nginx/default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

# copy artifact build from the 'build environment'
COPY --from=builder /usr/src/app/dist/moralis-angular /usr/share/nginx/html

# expose port 80
EXPOSE 80

# run nginx
ADD entrypoint.sh entrypoint.sh
RUN chmod +x entrypoint.sh

# Run app launcher
CMD ["sh", "./entrypoint.sh"]