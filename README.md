# MoralisAngular

This sample project is a minimal Angular application using [Moralis](https://moralis.io/) to connect to a crypto-wallet.

For the moment, we can :
- Log in with Metamask
- Log in with Ledger to an Elrond Wallet
- And see the current wallet balance and number of sent transactions.



## Prerequisites

To be able to run this project, please install :
- the latest LTS version of [Node](https://nodejs.org/en/download/)
- the latest version of Angular CLI (once Node is installed) : `npm install -g @angular/cli`

It's good to know the [Angular Basics](https://angular.io/start) .

And it's important to read and understand the [Moralis basics](https://docs.moralis.io/) . 


## Download/Install Npm dependencies

After cloning the project in your workspace, and before running it, install the Npm dependencies :
```
cd moralis-angular
npm install
```


## Start the application

Open the project with your favorite IDE and put your Moralis App Id and Moralis server Url in `src/environment/environment.ts` :
```
export const environment = {
  production: false,
  moralisAppId: "PUT YOUR APP ID HERE",
  moralisServerUrl: "PUT YOUR SERVER URL HERE"
};
```

Then, in a terminal, go into the moralis-angular project and start the live server :

```
cd moralis-angular
npm start
```

Then, you can open the application in your browser at [http://localhost:4200](http://localhost:4200) .

Important :
- To test the Metamask login, Metamask must be enabled in your browser.
- To test the Elrond wallet login with Ledger, Ledger live app must be installed on your computer, and your ledger key synchronized.


## Moralis integration

Moralis has been integrated by include the npm libraries ([see package.json](./package.json))
- web3
- moralis
- @elrondnetwork/erdjs

And by following [the Moralis doc](https://docs.moralis.io/) who is really precise and complete.


## Build the project

You can build the project by typing : `ng build`

Or, for production : `ng build --prod`. Be sure to have completed the `src/environment/environment-prod.ts` file before running a production build.

You will find the build static resources you have to copy to your web server (index.html, js, ...) in the `dist/moralis-angular` folder. 



## Build and Deploy with Docker

Additionnaly, if your are familiar with Docker and can deploy your application in production as a docker image, you can build it :
```
cd moralis-angular
docker build -t moralis-angular:1.0.0 .
```

And start it :
```
docker run -p 80:80 moralis-angular:1.0.0
```

The application is listening on the port 80. 

If deployed in local, you can open the application in your browser at [http://localhost:80](http://localhost:80) .